# Packet name example1 

[![Latest Version on Packagist](https://ilcine.gitlab.io/img/packagist.svg)](https://packagist.org/packages/emr/example1)
[![License](https://ilcine.gitlab.io/img/mit.svg)](LICENSE.md)

Simple example on laravel

_Emrullah İLÇİN_

- [Laravel Environmet Set](#Packet-name-example1)
    - [Installation](#installation)
    - [Usage](#usage)
   
## Installation

You can install the package with [Composer](https://packagist.org/packages/emr/example1) 

```bash
composer require emr/example1
```

## Usage

ip: localhost or your server ip; ex: My ip 94.177.187.77; 
port: blank port: ex 8000

```bash
RUN SERVER SIDE

php artisan serve --host=94.177.187.77 --port=8000
or 
php artisan serve --host=localhost --port=8000
```

```bash
RUN BROWSER

http://94.177.187.77:8000/audit/test1
or
http://localhost:8000/audit/test1
```