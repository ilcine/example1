<?php
namespace Emr\Example1;  // =src dizini; laraveldeki app 

use Illuminate\Support\ServiceProvider;
use Emr\Example1\Http\Controllers\Example1Controller;

class Example1ServiceProvider extends ServiceProvider
{

    public function boot()
    {
       // see:  ..  https://laravel.com/docs/5.7/packages
       $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
       $this->loadViewsFrom(__DIR__. '/../../resources/views', 'courier'); // Use `courier::` in the Controllers section
    }
    
    public function register()
    {
       // iptal //  $this->app->make('Emr\Example1\Http\Controllers\Example1Controller');
    }
    
}
